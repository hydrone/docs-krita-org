# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-21 13:31+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en colorcategory HDR Krita image OpenColor Deep Color\n"
"X-POFile-SpellExtra: Hi Kikilowbit IO images Rgbcolorcube True\n"

#: ../../general_concepts/colors/bit_depth.rst:None
msgid ".. image:: images/color_category/Kiki_lowbit.png"
msgstr ".. image:: images/color_category/Kiki_lowbit.png"

#: ../../general_concepts/colors/bit_depth.rst:1
msgid "Bit depth in Krita."
msgstr "A profundidade de cor no Krita."

#: ../../general_concepts/colors/bit_depth.rst:10
#: ../../general_concepts/colors/bit_depth.rst:15
msgid "Bit Depth"
msgstr "Profundidade de Cor"

#: ../../general_concepts/colors/bit_depth.rst:10
#: ../../general_concepts/colors/bit_depth.rst:26
msgid "Indexed Color"
msgstr "Cor Indexada"

#: ../../general_concepts/colors/bit_depth.rst:10
#: ../../general_concepts/colors/bit_depth.rst:46
msgid "Real Color"
msgstr "Cor Real"

#: ../../general_concepts/colors/bit_depth.rst:10
msgid "Color"
msgstr "Cor"

#: ../../general_concepts/colors/bit_depth.rst:10
msgid "Color Bit Depth"
msgstr "Profundidade de Cor em Bits"

#: ../../general_concepts/colors/bit_depth.rst:10
msgid "Deep Color"
msgstr "Cor Profunda"

#: ../../general_concepts/colors/bit_depth.rst:10
msgid "Floating Point Color"
msgstr "Cor de Vírgula Flutuante"

#: ../../general_concepts/colors/bit_depth.rst:10
msgid "Color Channels"
msgstr "Canais de Cores"

#: ../../general_concepts/colors/bit_depth.rst:17
msgid ""
"Bit depth basically refers to the amount of working memory per pixel you "
"reserve for an image."
msgstr ""
"A profundidade de cor em bits refere-se basicamente à quantidade de memória "
"funcional por cada pixel que reservar para uma imagem."

#: ../../general_concepts/colors/bit_depth.rst:19
msgid ""
"Like how having a A2 paper in real life can allow for much more detail in "
"the end drawing, it does take up more of your desk than a simple A4 paper."
msgstr ""
"Assim como o facto de um papel A2 na vida real poder permitir muito mais "
"detalhe no desenho final, também ocupa muito mais espaço da sua secretária "
"que um papel A4 simples."

#: ../../general_concepts/colors/bit_depth.rst:21
msgid ""
"However, this does not just refer to the size of the image, but also how "
"much precision you need per color."
msgstr ""
"Contudo, isto não se refere apenas ao tamanho da imagem, mas também à "
"precisão necessária que precisa para cada cor."

#: ../../general_concepts/colors/bit_depth.rst:23
msgid ""
"To illustrate this, I'll briefly talk about something that is not even "
"available in Krita:"
msgstr ""
"Para ilustrar isto, iremos falar brevemente sobre algo que não está sequer "
"disponível no Krita:"

#: ../../general_concepts/colors/bit_depth.rst:28
msgid ""
"In older programs, the computer would have per image, a palette that "
"contains a number for each color. The palette size is defined in bits, "
"because the computer can only store data in bit-sizes."
msgstr ""
"Nos programas mais antigos, o computador iria ter, por cada imagem, uma "
"paleta com um número por cada cor. O tamanho da paleta era definido em "
"'bits', porque o computador só consegue gravar dados em tamanhos de 'bits'."

#: ../../general_concepts/colors/bit_depth.rst:36
msgid "1bit"
msgstr "1bit"

#: ../../general_concepts/colors/bit_depth.rst:37
msgid "Only two colors in total, usually black and white."
msgstr "Só duas cores no total, sendo normalmente o preto e o branco."

#: ../../general_concepts/colors/bit_depth.rst:38
msgid "4bit (16 colors)"
msgstr "4 bits (16 cores)"

#: ../../general_concepts/colors/bit_depth.rst:39
msgid ""
"16 colors in total, these are famous as many early games were presented in "
"this color palette."
msgstr ""
"16 cores no total; estes eram famosos, porque existiam muitos jogos antigos "
"que eram apresentados com esta paleta."

#: ../../general_concepts/colors/bit_depth.rst:41
msgid "8bit"
msgstr "8 bits"

#: ../../general_concepts/colors/bit_depth.rst:41
msgid ""
"256 colors in total. 8bit images are commonly used in games to save on "
"memory for textures and sprites."
msgstr ""
"256 cores no total. As imagens a 8 bits são muito usadas nos jogos para "
"poupar memória nas texturas e imagens de movimento."

#: ../../general_concepts/colors/bit_depth.rst:43
msgid ""
"However, this is not available in Krita. Krita instead works with channels, "
"and counts how many colors per channel you need (described in terms of "
"''bits per channel''). This is called 'real color'."
msgstr ""
"Contudo, isto não está disponível no Krita. O Krita lida por sua vez com "
"canais, e conta quantas cores por canal precisa (descrito em termos de "
"''bits por canal''). Isto é chamado de 'cor real'."

#: ../../general_concepts/colors/bit_depth.rst:52
msgid ".. image:: images/color_category/Rgbcolorcube_3.png"
msgstr ".. image:: images/color_category/Rgbcolorcube_3.png"

#: ../../general_concepts/colors/bit_depth.rst:52
msgid ""
"1, 2, and 3bit per channel don't actually exist in any graphics application "
"out there, however, by imagining them, we can imagine how each bit affects "
"the precision: Usually, each bit subdivides each section in the color cube "
"meaning precision becomes a power of 2 bigger than the previous cube."
msgstr ""
"Os 1, 2, e 3 bits por canal não existem de facto em nenhuma aplicação "
"conhecida; contudo, se pudesse imaginá-las, poderia imaginar quanto é que "
"cada bit afectaria a precisão: normalmente, cada 'bit' sub-divide cada uma "
"das secções no cubo de cores, o que significa que a precisão fica uma "
"potência de 2 vezes maior que o cubo anterior."

#: ../../general_concepts/colors/bit_depth.rst:54
msgid "4bit per channel (not supported by Krita)"
msgstr "4 bits por canal (não suportado no Krita)"

#: ../../general_concepts/colors/bit_depth.rst:55
msgid ""
"Also known as Hi-Color, or 16bit color total. A bit of an old system, and "
"not used outside of specific displays."
msgstr ""
"Também conhecido por Hi-Color, ou cor de 16 bits no total. É um sistema "
"relativamente antigo e não é muito usado fora de alguns ecrãs específicos."

#: ../../general_concepts/colors/bit_depth.rst:56
msgid "8bit per channel"
msgstr "8 bits por canal"

#: ../../general_concepts/colors/bit_depth.rst:57
msgid ""
"Also known as \"True Color\", \"Millions of colors\" or \"24bit/32bit\". The "
"standard for many screens, and the lowest bit-depth Krita can handle."
msgstr ""
"Também conhecido por \"True Color\", \"Cor Verdadeira\", \"Milhões de cores"
"\" ou \"24 bits/32 bits\". Neste momento, é o padrão para muitos ecrãs e é a "
"menor profundidade de cor que o Krita consegue lidar."

#: ../../general_concepts/colors/bit_depth.rst:58
msgid "16bit per channel"
msgstr "16 bits por canal"

#: ../../general_concepts/colors/bit_depth.rst:59
msgid ""
"One step up from 8bit, 16bit per channel allows for colors that can't be "
"displayed by the screen. However, due to this, you are more likely to have "
"smoother gradients. Sometimes known as \"Deep Color\". This color depth type "
"doesn't have negative values possible, so it is 16bit precision, meaning "
"that you have 65536 values per channel."
msgstr ""
"Um passo à frente dos 8 bits, os 16 bits por canal permitem cores que ainda "
"não conseguem ser apresentadas pelo ecrã. Contudo, devido a este facto, será "
"capaz de ter gradientes mais suaves. Normalmente conhecido por \"Deep Color"
"\" (Cor Profunda). Este tipo de profundidade de cores não tem valores "
"negativos possíveis; como tal, tem uma precisão de 16 bits, o que significa "
"que pode ter 65536 valores por cada canal."

#: ../../general_concepts/colors/bit_depth.rst:60
msgid "16bit float"
msgstr "16 bits de vírgula flutuante"

#: ../../general_concepts/colors/bit_depth.rst:61
msgid ""
"Similar to 16bit, but with more range and less precision. Where 16bit only "
"allows coordinates like [1, 4, 3], 16bit float has coordinates like [0.15, "
"0.70, 0.3759], with [1.0,1.0,1.0] representing white. Because of the "
"differences between floating point and integer type variables, and because "
"Scene-referred imaging allows for negative values, you have about 10-11bits "
"of precision per channel in 16 bit floating point compared to 16 bit in 16 "
"bit int (this is 2048 values per channel in the 0-1 range). Required for HDR/"
"Scene referred images, and often known as 'half floating point'."
msgstr ""
"Semelhante aos 16 bits, com uma gama maior e com menos precisão. Onde os 16 "
"bits só permitem coordenadas do tipo [1, 4, 3], os 16 bits de vírgula "
"flutuante permitem coordenadas do tipo [0,15, 0,70, 0,3759], sendo que o "
"[1,0, 1,0, 1,0] representa o branco. Devido às diferenças entre as variáveis "
"de vírgula flutuante e os números inteiros, e dado que as imagens referentes "
"a cenas permitem valores negativos, terá cerca de 10-11 bits de precisão por "
"canal nos 16 bits de vírgula flutuante em comparação com 16 bits em 16 bits "
"inteiros (isso corresponde a 2048 valores por canal no intervalo 0-1). "
"Necessário para imagens referentes a cenas/HDR, e sendo conhecida por 'meia-"
"vírgula flutuante'."

#: ../../general_concepts/colors/bit_depth.rst:63
msgid ""
"Similar to 16bit float but with even higher precision. The native color "
"depth of OpenColor IO, and thus faster than 16bit float in HDR images, if "
"not heavier. Because of the nature of floating point type variables, 32bit "
"float is roughly equal to 23-24 bits of precision per channel (16777216 "
"values per channel in the 0-1 range), but with a much wider range (it can go "
"far above 1), necessary for HDR/Scene-referred values. It is also known as "
"'single floating point'."
msgstr ""
"semelhante aos 16 bits de vírgula flutuante, mas ainda com maior precisão. A "
"profundidade de cores nativa do OpenColor IO, pelo que será mais rápida que "
"os 16 bits de vírgula flutuante nas imagens HDR, se bem que mais pesada. "
"Devido à natureza das variáveis de vírgula flutuante, uma profundidade de 32 "
"bits de vírgula flutuante é aproximadamente igual a 23-24 bits de precisão "
"por canal (16777216 por canal, no intervalo de 0-1), mas com uma gama muito "
"mais ampla (pode ir acima de 1), o que é necessário para os valores "
"referentes a cenas/HDR. Também é conhecido por 'vírgula flutuante simples'."

#: ../../general_concepts/colors/bit_depth.rst:64
msgid "32bit float"
msgstr "32 bits de vírgula flutuante"

#: ../../general_concepts/colors/bit_depth.rst:66
msgid ""
"This is important if you have a working color space that is larger than your "
"device space: At the least, if you do not want color banding."
msgstr ""
"Isto é importante se tiver um espaço de cores de trabalho que seja maior que "
"o espaço do seu dispositivo: No mínimo, se não quiser qualquer separação por "
"bandas de cores."

#: ../../general_concepts/colors/bit_depth.rst:68
msgid ""
"And while you can attempt to create all your images a 32bit float, this will "
"quickly take up your RAM. Therefore, it's important to consider which bit "
"depth you will use for what kind of image."
msgstr ""
"E, embora possa tentar criar todas as suas imagens com uma profundidade de "
"32 bits de vírgula flutuante, isso irá consumir rapidamente a sua RAM. Como "
"tal, é importante considerar qual a profundidade de cor que irá usar para "
"cada tipo de imagem."
