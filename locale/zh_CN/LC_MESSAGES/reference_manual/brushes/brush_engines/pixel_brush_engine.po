msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___brushes___brush_engines___pixel_brush_engine."
"pot\n"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:1
msgid "The Pixel Brush Engine manual page."
msgstr "介绍 Krita 的像素笔刷引擎。"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:16
msgid "Pixel Brush Engine"
msgstr "像素笔刷引擎"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:11
msgid "Brush Engine"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:19
msgid ".. image:: images/icons/pixelbrush.svg"
msgstr ".. image:: images/icons/pixelbrush.svg"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:20
msgid ""
"Brushes are ordered alphabetically. The brush that is selected by default "
"when you start with Krita is the :guilabel:`Pixel Brush`. The pixel brush is "
"the traditional mainstay of digital art. This brush paints impressions of "
"the brush tip along your stroke with a greater or smaller density."
msgstr ""
"Krita 的笔刷列表是按照字母进行排序的。你启动 Krita 后默认选中的是一个 :"
"guilabel:`像素笔刷` 。像素笔刷是数字美术的必备工具。它在你的笔迹上按照压力留"
"下或大或小，或硬或软的笔画。"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:24
msgid ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Popup.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:25
msgid "Let's first review these mechanics:"
msgstr "现在让我们复习一下像素笔刷的工作机制："

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:27
msgid ""
"Select a brush tip. This can be a generated brush tip (round, square, star-"
"shaped), a predefined bitmap brush tip, a custom brush tip or a text."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:28
msgid ""
"Select the spacing: this determines how many impressions of the tip will be "
"made along your stroke"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:29
msgid ""
"Select the effects: the pressure of your stylus, your speed of painting or "
"other inputs can change the size, the color, the opacity or other aspects of "
"the currently painted brush tip instance -- some applications call that a "
"\"dab\"."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:30
msgid ""
"Depending on the brush mode, the previously painted brush tip instance is "
"mixed with the current one, causing a darker, more painterly stroke, or the "
"complete stroke is computed and put on your layer. You will see the stroke "
"grow while painting in both cases, of course!"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:32
msgid ""
"Since 4.0, the Pixel Brush Engine has Multithreaded brush-tips, with the "
"default brush being the fastest mask."
msgstr ""
"像素笔刷引擎从 Krita 4.0 版开始使用多线程笔尖，而“默认”笔尖蒙版的处理速度是三"
"种蒙版中最快的。"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:34
msgid "Available Options:"
msgstr "可用笔刷选项："

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:36
msgid ":ref:`option_brush_tip`"
msgstr ":ref:`option_brush_tip`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:37
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:38
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:39
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:40
msgid ":ref:`option_ratio`"
msgstr ":ref:`option_ratio`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:41
msgid ":ref:`option_spacing`"
msgstr ":ref:`option_spacing`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:42
msgid ":ref:`option_mirror`"
msgstr ":ref:`option_mirror`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:43
msgid ":ref:`option_softness`"
msgstr ":ref:`option_softness`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:44
msgid ":ref:`option_sharpness`"
msgstr ":ref:`option_sharpness`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:45
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:46
msgid ":ref:`option_scatter`"
msgstr ":ref:`option_scatter`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:47
msgid ":ref:`option_source`"
msgstr ":ref:`option_source`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:48
msgid ":ref:`option_mix`"
msgstr ":ref:`option_mix`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:49
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:50
msgid ":ref:`option_texture`"
msgstr ":ref:`option_texture`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:51
msgid ":ref:`option_masked_brush`"
msgstr ":ref:`option_masked_brush`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:54
msgid "Specific Parameters to the Pixel Brush Engine"
msgstr "像素笔刷引擎的特有选项"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:57
msgid "Darken"
msgstr "变暗"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:59
msgid "Allows you to Darken the source color with Sensors."
msgstr "此选项控制传感器对来源颜色变暗效果的影响。"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:62
msgid ".. image:: images/brushes/Krita_2_9_brushengine_darken_01.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:63
msgid ""
"The color will always become black in the end, and will work with Plain "
"Color, Gradient and Uniform random as source."
msgstr ""
"启用变暗选项后，笔刷的颜色会越画越暗，直到变黑。此选项可以与纯色、渐变和笔尖"
"随机等来源颜色配合使用。"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:66
msgid "Hue, Saturation, Value"
msgstr "色相、饱和度、明度"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:68
msgid ""
"These parameters allow you to do an HSV adjustment filter on the :ref:"
"`option_source` and control it with Sensors."
msgstr ""
"这些选项可以在 :ref:`option_source` 颜色上面应用 HSV 调整滤镜，并通过传感器控"
"制其效果。"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:71
msgid ".. image:: images/brushes/Krita_2_9_brushengine_HSV_01.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:72
msgid "Works with Plain Color, Gradient and Uniform random as source."
msgstr "它们可以和纯色、渐变和笔尖随机等来源颜色配合使用。"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:75
msgid "Uses"
msgstr "用例"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:78
msgid ".. image:: images/brushes/Krita_2_9_brushengine_HSV_02.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:79
msgid ""
"Having all three parameters on Fuzzy will help with rich color texture. In "
"combination with :ref:`option_mix`, you can have even finer control."
msgstr ""
"将这三个选项配合随机度传感器使用可以绘制出颜色丰富多变的肌理。你还可以把它们"
"与 :ref:`option_mix` 选项配合使用，进行更细腻的控制。"
