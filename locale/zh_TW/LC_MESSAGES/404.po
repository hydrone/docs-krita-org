# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# pan93412 <pan93412@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-04 20:25+0800\n"
"Last-Translator: pan93412 <pan93412@gmail.com>\n"
"Language-Team: Chinese <zh-l10n@linux.org.tw>\n"
"Language: zh\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 18.12.2\n"

#: ../../404.rst:None
#, fuzzy
#| msgid ""
#| ".. image:: images/en/color_category/Kiki_cLUTprofiles.png\n"
#| "   :alt: Image of Kiki looking confused through books."
msgid ""
".. image:: images/color_category/Kiki_cLUTprofiles.png\n"
"   :alt: Image of Kiki looking confused through books."
msgstr ""
".. image:: images/en/color_category/Kiki_cLUTprofiles.png\n"
"   :alt: Image of Kiki looking confused through books."

#: ../../404.rst:5
msgid "File Not Found (404)"
msgstr "找不到檔案 (404)"

#: ../../404.rst:10
msgid "This page does not exist."
msgstr "此頁面不存在。"

#: ../../404.rst:12
msgid "This might be because of the following:"
msgstr "可能是因為這些原因所致："

#: ../../404.rst:14
msgid ""
"We moved the manual from MediaWiki to Sphinx and with that came a lot of "
"reorganization."
msgstr ""

#: ../../404.rst:15
msgid "The page has been deprecated."
msgstr "此頁面已經廢棄。"

#: ../../404.rst:16
msgid "Or a simple typo in the url."
msgstr "或網址中有個小錯字。"

#: ../../404.rst:18
msgid ""
"In all cases, you might be able to find the page you're looking for by using "
"the search in the lefthand navigation."
msgstr ""
