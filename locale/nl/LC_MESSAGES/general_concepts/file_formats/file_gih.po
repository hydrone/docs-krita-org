# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-06-17 10:59+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../general_concepts/file_formats/file_gih.rst:1
msgid "The Gimp Image Hose file format in Krita."
msgstr "Het bestandsformaat Gimp Image Hose in Krita."

#: ../../general_concepts/file_formats/file_gih.rst:10
msgid "Image Hose"
msgstr "Image Hose"

#: ../../general_concepts/file_formats/file_gih.rst:10
msgid "Gimp Image Hose"
msgstr "Gimp Image Hose"

#: ../../general_concepts/file_formats/file_gih.rst:10
msgid "GIH"
msgstr "GIH"

#: ../../general_concepts/file_formats/file_gih.rst:10
msgid "*.gih"
msgstr "*.gih"

#: ../../general_concepts/file_formats/file_gih.rst:15
msgid "\\*.gih"
msgstr "\\*.gih"

#: ../../general_concepts/file_formats/file_gih.rst:17
msgid ""
"The GIMP image hose format. Krita can open and save these, as well as import "
"via the :ref:`predefined brush tab <predefined_brush_tip>`."
msgstr ""
"Het formaat image-hose van GIMP. Krita kan deze openen en opslaan, evenals "
"ze importeren via het :ref:`tabblad voorgedefinieerd penseel "
"<predefined_brush_tip>`."

#: ../../general_concepts/file_formats/file_gih.rst:19
msgid ""
"Image Hose means that this file format allows you to store multiple images "
"and then set some options to make it specify how to output the multiple "
"images."
msgstr ""
"Image-hose betekent dat dit bestandsformaat u toestaat om meerdere "
"afbeeldingen op te slaan en daarna enige opties in te stellen om te "
"specificeren hoe de meerdere afbeeldingen worden uitgevoerd."

#: ../../general_concepts/file_formats/file_gih.rst:25
msgid ".. image:: images/brushes/Gih-examples.png"
msgstr ".. image:: images/brushes/Gih-examples.png"

#: ../../general_concepts/file_formats/file_gih.rst:25
msgid "From top to bottom: Incremental, Pressure and Random"
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:27
msgid "Gimp image hose format options:"
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:29
msgid "Constant"
msgstr "Constant"

#: ../../general_concepts/file_formats/file_gih.rst:30
msgid "This'll use the first image, no matter what."
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:31
msgid "Incremental"
msgstr "Incrementeel"

#: ../../general_concepts/file_formats/file_gih.rst:32
msgid ""
"This'll paint the image layers in sequence. This is good for images that can "
"be strung together to create a pattern."
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:33
msgid "Pressure"
msgstr "Druk"

#: ../../general_concepts/file_formats/file_gih.rst:34
msgid ""
"This'll paint the images depending on pressure. This is good for brushes "
"imitating the hairs of a natural brush."
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:35
msgid "Random"
msgstr "Willekeurig"

#: ../../general_concepts/file_formats/file_gih.rst:36
msgid ""
"This'll draw the images randomly. This is good for image-collections used in "
"speedpainting as well as images that generate texture. Or perhaps more "
"graphical symbols."
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:38
msgid "Angle"
msgstr "Hoek"

#: ../../general_concepts/file_formats/file_gih.rst:38
msgid "This'll use the dragging angle to determine with image to draw."
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:40
msgid ""
"When exporting a Krita file as a ``.gih``, you will also get the option to "
"set the default spacing, the option to set the name (very important for "
"looking it up in the UI) and the ability to choose whether or not to "
"generate the mask from the colors."
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:43
msgid "Use Color as Mask"
msgstr "Kleur als masker gebruiken"

#: ../../general_concepts/file_formats/file_gih.rst:43
msgid ""
"This'll turn the darkest values of the image as the ones that paint, and the "
"whitest as transparent. Untick this if you are using colored images for the "
"brush."
msgstr ""

#: ../../general_concepts/file_formats/file_gih.rst:45
msgid ""
"We have a :ref:`Krita Brush tip page <brush_tip_animated_brush>` on how to "
"create your own gih brush."
msgstr ""
