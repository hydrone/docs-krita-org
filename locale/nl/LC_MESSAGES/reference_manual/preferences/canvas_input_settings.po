# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-25 13:23+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.2\n"

#: ../../<generated>:1
msgid "Profile"
msgstr "Profiel"

#: ../../reference_manual/preferences/canvas_input_settings.rst:1
msgid "Canvas input settings in Krita."
msgstr "Invoerinstellingen voor werkblad in Krita."

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
#: ../../reference_manual/preferences/canvas_input_settings.rst:16
msgid "Canvas Input Settings"
msgstr "Invoerinstellingen voor werkblad"

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
msgid "Preferences"
msgstr ""

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
msgid "Settings"
msgstr ""

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
msgid "Tablet"
msgstr ""

#: ../../reference_manual/preferences/canvas_input_settings.rst:18
msgid ""
"Krita has ways to set mouse and keyboard combinations for different actions. "
"The user can set which combinations to use for a certain Krita command over "
"here. This section is under development and will include more options in "
"future."
msgstr ""

#: ../../reference_manual/preferences/canvas_input_settings.rst:21
msgid "The user can make different profiles of combinations and save them."
msgstr ""
