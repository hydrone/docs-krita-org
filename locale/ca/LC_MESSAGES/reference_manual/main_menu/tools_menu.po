# Translation of docs_krita_org_reference_manual___main_menu___tools_menu.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-09 10:29+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../reference_manual/main_menu/tools_menu.rst:1
msgid "The tools menu in Krita."
msgstr "El menú Eines en el Krita."

#: ../../reference_manual/main_menu/tools_menu.rst:11
msgid "Macro"
msgstr "Macro"

#: ../../reference_manual/main_menu/tools_menu.rst:11
msgid "Scripts"
msgstr "Scripts"

#: ../../reference_manual/main_menu/tools_menu.rst:17
msgid "Tools Menu"
msgstr "El menú Eines"

#: ../../reference_manual/main_menu/tools_menu.rst:19
msgid "This contains three things."
msgstr "Conté tres coses."

#: ../../reference_manual/main_menu/tools_menu.rst:22
msgid "Scripting"
msgstr "Crear scripts"

#: ../../reference_manual/main_menu/tools_menu.rst:24
msgid ""
"When you have python scripting enabled and have scripts toggled, this is "
"where most scripts are stored by default."
msgstr ""
"Quan teniu habilitat la creació de scripts en Python i teniu scripts "
"activats, aquí és on s'emmagatzemaran de forma predeterminada la majoria de "
"scripts."

#: ../../reference_manual/main_menu/tools_menu.rst:27
msgid "Recording"
msgstr "Enregistrar"

#: ../../reference_manual/main_menu/tools_menu.rst:31
msgid "The recording and macro features are unmaintained and buggy."
msgstr ""
"Les característiques d'enregistrament i de macro no es mantenen i contenen "
"errors."

#: ../../reference_manual/main_menu/tools_menu.rst:33
msgid ""
"Record a macro. You do this by pressing start, drawing something and then "
"pressing stop. This feature can only record brush strokes. The resulting "
"file is stored as a \\*.kritarec file."
msgstr ""
"Per enregistrar una macro. Feu això prement Inicia, dibuixeu quelcom i "
"després premeu Atura. Aquesta característica només pot enregistrar les "
"pinzellades. El fitxer resultant s'emmagatzemarà com un fitxer «\\*."
"kritarec»."

#: ../../reference_manual/main_menu/tools_menu.rst:36
msgid "Macros"
msgstr "Macros"

#: ../../reference_manual/main_menu/tools_menu.rst:38
msgid ""
"Play back or edit a krita rec file. The edit can only change the brush "
"preset on strokes or add and remove filters."
msgstr ""
"Reprodueix o edita un fitxer d'enregistrament del Krita. L'edició només "
"podrà canviar el pinzell predefinit en els traços o afegir i eliminar "
"filtres."
