# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-07-02 22:48+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/blending_modes/arithmetic.rst:1
msgid ""
"Page about the arithmetic blending modes in Krita: Addition, Divide, Inverse "
"Subtract, Multiply and Subtract."
msgstr ""
"Sida om aritmetiska blandningslägen i Krita: Addition, Division, Invers "
"subtraktion, Multiplikation och Subtraktion."

#: ../../reference_manual/blending_modes/arithmetic.rst:14
msgid "Arithmetic"
msgstr "Aritmetik"

#: ../../reference_manual/blending_modes/arithmetic.rst:16
msgid "These blending modes are based on simple maths."
msgstr "Dessa blandningslägen är baserade på enkel matematik."

#: ../../reference_manual/blending_modes/arithmetic.rst:18
msgid "Addition (Blending Mode)"
msgstr "Addition (blandningsläge)"

#: ../../reference_manual/blending_modes/arithmetic.rst:22
msgid "Addition"
msgstr "Addition"

#: ../../reference_manual/blending_modes/arithmetic.rst:24
msgid "Adds the numerical values of two colors together:"
msgstr "Summerar de numeriska värdena av två färger:"

#: ../../reference_manual/blending_modes/arithmetic.rst:26
msgid "Yellow(1, 1, 0) + Blue(0, 0, 1) = White(1, 1, 1)"
msgstr "Gul(1, 1, 0) + Blå(0, 0, 1) = Vit(1, 1, 1)"

#: ../../reference_manual/blending_modes/arithmetic.rst:28
msgid ""
"Darker Gray(0.4, 0.4, 0.4) + Lighter Gray(0.5, 0.5, 0.5) = Even Lighter Gray "
"(0.9, 0.9, 0.9)"
msgstr ""
"Mörkgrå(0.4, 0.4, 0.4) + ljusgrå(0.5, 0.5, 0.5)] = Ännu ljusare grå(0.9, "
"0.9, 0.9)"

#: ../../reference_manual/blending_modes/arithmetic.rst:33
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Addition_Gray_0.4_and_Gray_0.5_n.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Addition_Gray_0.4_and_Gray_0.5_n.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:33
#: ../../reference_manual/blending_modes/arithmetic.rst:40
#: ../../reference_manual/blending_modes/arithmetic.rst:47
#: ../../reference_manual/blending_modes/arithmetic.rst:54
msgid "Left: **Normal**. Right: **Addition**."
msgstr "Vänster: **Normal**. Höger: **Addition**."

#: ../../reference_manual/blending_modes/arithmetic.rst:35
msgid ""
"Light Blue(0.1608, 0.6274, 0.8274) + Orange(1, 0.5961, 0.0706) = (1.1608, "
"1.2235, 0.8980) → Very Light Yellow(1, 1, 0.8980)"
msgstr ""
"Ljusblå(0.1608, 0.6274, 0.8274) + Orange(1, 0.5961, 0.0706) = (1.1608, "
"1.2235, 0.8980) → Mycket ljus gul(1, 1, 0.8980)"

#: ../../reference_manual/blending_modes/arithmetic.rst:40
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Addition_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Addition_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:42
msgid "Red(1, 0, 0) + Gray(0.5, 0.5, 0.5) = Pink(1, 0.5, 0.5)"
msgstr "Röd(1, 0, 0) + Grå(0.5, 0.5, 0.5) = Rosa(1, 0.5, 0.5)"

#: ../../reference_manual/blending_modes/arithmetic.rst:47
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Addition_Red_plus_gray.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Addition_Red_plus_gray.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:49
msgid ""
"When the result of the addition is more than 1, white is the color "
"displayed. Therefore, white plus any other color results in white. On the "
"other hand, black plus any other color results in the added color."
msgstr ""
"När resultatet av additionen är mer än 1, är vitt färgen som visas. Därför "
"ger vitt plus vilken annan färg som helst vitt. Å andra sidan ger svart plus "
"vilken annan färg som helst den tillagda färgen."

#: ../../reference_manual/blending_modes/arithmetic.rst:54
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Addition_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Addition_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:59
msgid "Divide"
msgstr "Division"

#: ../../reference_manual/blending_modes/arithmetic.rst:61
msgid "Divides the numerical value from the lower color by the upper color."
msgstr ""
"Dividerar det numeriska värdet från den undre färgen med den övre färgen."

#: ../../reference_manual/blending_modes/arithmetic.rst:63
msgid "Red(1, 0, 0) / Gray(0.5, 0.5, 0.5) = (2, 0, 0) → Red(1, 0, 0)"
msgstr "Röd(1, 0, 0) / Grå(0.5, 0.5, 0.5) = (2, 0, 0) → Röd(1, 0, 0)"

#: ../../reference_manual/blending_modes/arithmetic.rst:65
msgid ""
"Darker Gray(0.4, 0.4, 0.4) / Lighter Gray(0.5, 0.5, 0.5) = Even Lighter Gray "
"(0.8, 0.8, 0.8)"
msgstr ""
"Mörkgrå(0.4, 0.4, 0.4) / Ljusgrå(0.5, 0.5, 0.5)] = Ännu ljusare grå(0.8, "
"0.8, 0.8)"

#: ../../reference_manual/blending_modes/arithmetic.rst:70
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Divide_Gray_0.4_and_Gray_0.5_n.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Divide_Gray_0.4_and_Gray_0.5_n.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:70
#: ../../reference_manual/blending_modes/arithmetic.rst:77
#: ../../reference_manual/blending_modes/arithmetic.rst:82
msgid "Left: **Normal**. Right: **Divide**."
msgstr "Vänster: **Normal**. Höger: **Dividera**."

#: ../../reference_manual/blending_modes/arithmetic.rst:72
msgid ""
"Light Blue(0.1608, 0.6274, 0.8274) / Orange(1, 0.5961, 0.0706) = (0.1608, "
"1.0525, 11.7195) → Aqua(0.1608, 1, 1)"
msgstr ""
"Ljusblå(0.608, 0.6274, 0.8274) / Orange(1, 0.5961, 0.0706)] = (0.1608, "
"1.0525, 11.7195) → Turkos(0.1608, 1, 1)"

#: ../../reference_manual/blending_modes/arithmetic.rst:77
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Divide_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Divide_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:82
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Divide_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Divide_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:87
msgid "Inverse Subtract"
msgstr "Omvänd subtraktion"

#: ../../reference_manual/blending_modes/arithmetic.rst:89
msgid ""
"This inverts the lower layer before subtracting it from the upper layer."
msgstr ""
"Inverterar det undre lagret innan det subtraheras från det övre lagret."

#: ../../reference_manual/blending_modes/arithmetic.rst:91
msgid ""
"Lighter Gray(0.5, 0.5, 0.5)_(1_Darker Gray(0.4, 0.4, 0.4)) = (-0.1, -0.1, "
"-0.1) → Black(0, 0, 0)"
msgstr ""
"Ljusgrå(0.5, 0.5, 0.5)_(1_Mörkgrå(0.4, 0.4, 0.4)) = (-0.1, -0.1, -0.1) → "
"Svart(0, 0, 0)"

#: ../../reference_manual/blending_modes/arithmetic.rst:96
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Inverse_Subtract_Gray_0.4_and_Gray_0.5_n.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Inverse_Subtract_Gray_0.4_and_Gray_0.5_n.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:96
#: ../../reference_manual/blending_modes/arithmetic.rst:103
#: ../../reference_manual/blending_modes/arithmetic.rst:108
msgid "Left: **Normal**. Right: **Inverse Subtract**."
msgstr "Vänster: **Normal**. Höger: **Omvänd subtraktion**."

#: ../../reference_manual/blending_modes/arithmetic.rst:98
msgid ""
"Orange(1, 0.5961, 0.0706)_(1_Light Blue(0.1608, 0.6274, 0.8274)) = (0.1608, "
"0.2235, -0.102) → Dark Green(0.1608, 0.2235, 0)"
msgstr ""
"Orange(1, 0.5961, 0.0706)_(1_Ljusblå(0.1608, 0.6274, 0.8274)) = (0.1608, "
"0.2235, -0.102) → Mörkgrön(0.1608, 0.2235, 0)"

#: ../../reference_manual/blending_modes/arithmetic.rst:103
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Inverse_Subtract_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Inverse_Subtract_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:108
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Inverse_Subtract_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Inverse_Subtract_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:113
msgid "Multiply"
msgstr "Multiplicera"

#: ../../reference_manual/blending_modes/arithmetic.rst:115
msgid ""
"Multiplies the two colors with each other, but does not go beyond the upper "
"limit."
msgstr ""
"Multiplicerar de två färgerna med varandra, men går inte förbi den övre "
"gränsen."

#: ../../reference_manual/blending_modes/arithmetic.rst:117
msgid ""
"This is often used to color in a black and white lineart. One puts the black "
"and white lineart on top, and sets the layer to 'Multiply', and then draw in "
"color on a layer beneath. Multiply will all the color to go through."
msgstr ""
"Det används ofta för att färglägga i en svartvit teckning. Man placerar den "
"svartvita teckningen överst, ställer in lagret till 'Multiplicera' och ritar "
"därefter i färg på ett lager under. Multiplicera låter alla färger skina "
"igenom."

#: ../../reference_manual/blending_modes/arithmetic.rst:120
msgid "White(1,1,1) x White(1, 1, 1) = White(1, 1, 1)"
msgstr "Vit(1,1,1) x Vit(1, 1, 1) = Vit(1, 1, 1)"

#: ../../reference_manual/blending_modes/arithmetic.rst:122
msgid "White(1, 1, 1) x Gray(0.5, 0.5, 0.5) = Gray(0.5, 0.5, 0.5)"
msgstr "Vit(1, 1, 1) x Grå(0.5, 0.5, 0.5) = Grå(0.5, 0.5, 0.5)"

#: ../../reference_manual/blending_modes/arithmetic.rst:124
msgid ""
"Darker Gray(0.4, 0.4, 0.4) x Lighter Gray(0.5, 0.5, 0.5) = Even Darker Gray "
"(0.2, 0.2, 0.2)"
msgstr ""
"Mörkgrå(0.4, 0.4, 0.4) x Ljusgrå(0.5, 0.5, 0.5)] = Ännu mörkare grå(0.2, "
"0.2, 0.2)"

#: ../../reference_manual/blending_modes/arithmetic.rst:129
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Multiply_Gray_0.4_and_Gray_0.5_n.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Multiply_Gray_0.4_and_Gray_0.5_n.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:129
#: ../../reference_manual/blending_modes/arithmetic.rst:136
#: ../../reference_manual/blending_modes/arithmetic.rst:141
msgid "Left: **Normal**. Right: **Multiply**."
msgstr "Vänster: **Normal**. Höger: **Multiplicera**."

#: ../../reference_manual/blending_modes/arithmetic.rst:131
msgid ""
"Light Blue(0.1608, 0.6274, 0.8274) x Orange(1, 0.5961, 0.0706) = "
"Green(0.1608, 0.3740, 0.0584)"
msgstr ""
"Ljusblå(0.1608, 0.6274, 0.8274) x Orange(1, 0.5961, 0.0706) = Grön(0.1608, "
"0.3740, 0.0584)"

#: ../../reference_manual/blending_modes/arithmetic.rst:136
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Multiply_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Multiply_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:141
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Multiply_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Multiply_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:146
msgid "Subtract"
msgstr "Subtrahera"

#: ../../reference_manual/blending_modes/arithmetic.rst:148
msgid "Subtracts the top layer from the bottom layer."
msgstr "Subtraherar det övre lagret från det undre lagret."

#: ../../reference_manual/blending_modes/arithmetic.rst:150
msgid "White(1, 1, 1)_White(1, 1, 1) = Black(0, 0, 0)"
msgstr "Vit(1, 1, 1)_Vit(1, 1, 1) = Svart(0, 0, 0)"

#: ../../reference_manual/blending_modes/arithmetic.rst:152
msgid "White(1, 1, 1)_Gray(0.5, 0.5, 0.5) = Gray(0.5, 0.5, 0.5)"
msgstr "Vit(1, 1, 1)_Grå(0.5, 0.5, 0.5) = Grå(0.5, 0.5, 0.5)"

#: ../../reference_manual/blending_modes/arithmetic.rst:154
msgid ""
"Darker Gray(0.4, 0.4, 0.4)_Lighter Gray(0.5, 0.5, 0.5) = (-0.1, -0.1, -0.1) "
"→ Black(0, 0, 0)"
msgstr ""
"Mörkgrå(0.4, 0.4, 0.4)_Ljusgrå(0.5, 0.5, 0.5) = (-0.1, -0.1, -0.1) → "
"Svart(0, 0, 0)"

#: ../../reference_manual/blending_modes/arithmetic.rst:159
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Subtract_Gray_0.4_and_Gray_0.5_n.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Subtract_Gray_0.4_and_Gray_0.5_n.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:159
#: ../../reference_manual/blending_modes/arithmetic.rst:166
#: ../../reference_manual/blending_modes/arithmetic.rst:171
msgid "Left: **Normal**. Right: **Subtract**."
msgstr "Vänster: **Normal**. Höger: **Subtrahera**."

#: ../../reference_manual/blending_modes/arithmetic.rst:161
msgid ""
"Light Blue(0.1608, 0.6274, 0.8274) - Orange(1, 0.5961, 0.0706) = (-0.8392, "
"0.0313, 0.7568) → Blue(0, 0.0313, 0.7568)"
msgstr ""
"Ljusblå(0.1608, 0.6274, 0.8274) - Orange(1, 0.5961, 0.0706) = (-0.8392, "
"0.0313, 0.7568) → Blå(0, 0.0313, 0.7568)"

#: ../../reference_manual/blending_modes/arithmetic.rst:166
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Subtract_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Subtract_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/arithmetic.rst:171
msgid ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Subtract_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/arithmetic/"
"Blending_modes_Subtract_Sample_image_with_dots.png"
