# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:09+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: vänsterknapp"

#: ../../reference_manual/instant_preview.rst:1
msgid "How the Instant Preview technology in Krita works."
msgstr "Hur teknologin för direkt förhandsgranskning i Krita fungerar."

#: ../../reference_manual/instant_preview.rst:10
#: ../../reference_manual/instant_preview.rst:15
msgid "Instant Preview"
msgstr "Direkt förhandsgranskning"

#: ../../reference_manual/instant_preview.rst:10
msgid "Performance"
msgstr "Prestanda"

#: ../../reference_manual/instant_preview.rst:10
msgid "Lag"
msgstr "Fördröjning"

#: ../../reference_manual/instant_preview.rst:17
msgid ""
"Instant Preview (previously known under the code name Level Of Detail/LOD "
"strokes) is Krita's special speed-up mechanism that was funded by the 2015 "
"Kickstarter. Krita slows down with really large images due to the large "
"amount of data it's crunching in painting these images. Instant Preview "
"works by taking a smaller version of the canvas, and drawing the feedback on "
"there while Krita calculates the real stroke in the background. This means "
"that if you have a 4k screen and are working on a 4k image at 100% zoom, you "
"won't feel any speed up."
msgstr ""
"Direkt förhandsgranskning (tidigare känt under kodnamnet Level Of Detail "
"eller LOD-penseldrag) är Kritas speciella mekanism för uppsnabbning som "
"finansierades av Kickstarter 2015. Krita blir långsammare med mycket stora "
"bilder på grund av den stora mängd data som behandlas när bilderna målas. "
"Direkt förhandsgranskning fungerar genom att ta en mindre version av duken "
"och rita återkoppling där medan Krita beräknar det verkliga penseldraget i "
"bakgrunden. Det betyder att om man har en 4K bildskärm och arbetar med en 4K "
"bild och 100 % zoomnivå, märker man ingen uppsnabbning."

#: ../../reference_manual/instant_preview.rst:20
msgid "Activating Instant Preview"
msgstr "Aktivera direkt förhandsgranskning"

#: ../../reference_manual/instant_preview.rst:24
msgid ""
"Instant Preview requires OpenGL 3.0 support at minimum. So if you don't "
"have :guilabel:`high-quality` scaling available in :menuselection:`Settings "
"--> Configure Krita --> Display --> Display scaling filter`, then you won't "
"be able to use Instant Preview either."
msgstr ""
"Direkt förhandsgranskning kräver som minst stöd för OpenGL 3.0. Om man inte "
"har skalning med :guilabel:`hög kvalitet` tillgänglig under :menuselection:"
"`Inställningar --> Anpassa Krita --> Bildskärm --> Skalningsfilter för "
"bildskärm`, kan man inte heller använda direkt förhandsgranskning."

#: ../../reference_manual/instant_preview.rst:29
msgid ".. image:: images/brushes/Lod_position.png"
msgstr ".. image:: images/brushes/Lod_position.png"

#: ../../reference_manual/instant_preview.rst:29
msgid "The Global Instant Preview toggle is under the view menu."
msgstr ""
"Den globala inställningen av direkt förhandsgranskning finns i menyn Visa."

#: ../../reference_manual/instant_preview.rst:31
msgid ""
"Instant Preview is activated in two places: The view menu (:kbd:`Shift + L` "
"shortcut), and the settings of the given paintop by default. This is because "
"Instant Preview has different limitations with different paint operations."
msgstr ""
"Direkt förhandsgranskning aktiveras på två ställen. Menyn Visa (genvägen :"
"kbd:`Skift + L`) och i normalt inställningarna av given målaråtgärd. Det "
"beror på att direkt förhandsgranskning har olika begränsningar för olika "
"målaråtgärder."

#: ../../reference_manual/instant_preview.rst:33
msgid ""
"For example, the overlay mode in the color smudge brush will disable the "
"ability to have Instant Preview on the brush, so does using 'fade' sensor "
"for size."
msgstr ""
"Exempelvis inaktiverar överlagringsläget i färgsmetningspenseln möjlighet "
"att få direkt förhandsgranskning för penseln, och det gör även användning av "
"sensorn 'tona' för storlek."

#: ../../reference_manual/instant_preview.rst:35
msgid ""
"Similarly, the auto-spacing, fuzzy sensor in size, use of density in brush-"
"tip and the use of texture paintops will make it more difficult to determine "
"a stroke, and thus will give a feeling of 'popping' when the stroke is "
"finished."
msgstr ""
"På liknande sätt blir det svårare att bestämma ett drag med automatiskt "
"mellanrum, suddig storlekssensor, vid användning av täthet i penselspetsen "
"och vid användning av struktur i målaråtgärder, och sålunda får man en "
"känsla av 'ryckighet' när draget avslutas."

#: ../../reference_manual/instant_preview.rst:37
msgid ""
"When you check the brush settings, the Instant Preview checkbox will have a "
"\\* behind it. Hovering over it will give you a list of options that are "
"affecting the Instant Preview mode."
msgstr ""
"När man tittar på penselinställningarna följs kryssrutan för direkt "
"förhandsgranskning av en \\* . Genom att hålla musen över den, visas en "
"lista över alternativ som påverkar direkt förhandsgranskning."

#: ../../reference_manual/instant_preview.rst:41
msgid ""
"|mouseleft| this pop-up will give a slider, which can be used to determine "
"the threshold size at which instant preview activates. By default this "
"100px. This is useful for brushes that are optimised to work on small sizes."
msgstr ""
"Vänster musknapp visar en ruta med skjutreglage som kan användas för att "
"bestämma tröskelstorleken då direkt förhandsgranskning aktiveras. Normalt är "
"det 100 bildpunkter. Det är användbart för penslar som är optimerade för att "
"arbeta med små storlekar."

#: ../../reference_manual/instant_preview.rst:47
msgid ".. image:: images/brushes/Lod_position2.png"
msgstr ".. image:: images/brushes/Lod_position2.png"

#: ../../reference_manual/instant_preview.rst:47
msgid ""
"The Instant Preview checkbox at the bottom of the brush settings editor will "
"give you feedback when there's settings active that can't be previewed "
"right. Hover over it to get more detail. In this case, the issue is that "
"auto-spacing is on."
msgstr ""
"Kryssrutan för direkt förhandsgranskning längst ner i "
"penselinställningseditorn ger återmatning när det finns aktiverade "
"inställningar som inte kan förhandsgranskas på riktigt sätt. Håll musen över "
"den för att få mer detaljerad information. I det här fallet är problemet att "
"automatiskt mellanrum är aktiverat."

#: ../../reference_manual/instant_preview.rst:50
msgid "Tools that benefit from Instant Preview"
msgstr "Verktyg som drar nytta av direkt förhandsgranskning"

#: ../../reference_manual/instant_preview.rst:52
msgid "The following tools benefit from Instant Preview:"
msgstr "Följande verktyg drar nytta av direkt förhandsgranskning:"

#: ../../reference_manual/instant_preview.rst:54
msgid "The Freehand brush tool."
msgstr "Frihandspenselverktyget."

#: ../../reference_manual/instant_preview.rst:55
msgid "The geometric tools."
msgstr "De geometriska verktygen."

#: ../../reference_manual/instant_preview.rst:56
msgid "The Move Tool."
msgstr "Flyttningsverktyget."

#: ../../reference_manual/instant_preview.rst:57
msgid "The Filters."
msgstr "Filtren."

#: ../../reference_manual/instant_preview.rst:58
msgid "Animation."
msgstr "Animering."
