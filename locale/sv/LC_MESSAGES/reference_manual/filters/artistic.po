# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 16:19+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/filters/artistic.rst:1
msgid "Overview of the artistic filters."
msgstr "Översikt av de konstnärliga filtren."

#: ../../reference_manual/filters/artistic.rst:11
#: ../../reference_manual/filters/artistic.rst:21
msgid "Halftone"
msgstr "Halvton"

#: ../../reference_manual/filters/artistic.rst:11
msgid "Filters"
msgstr "Filter"

#: ../../reference_manual/filters/artistic.rst:11
msgid "HD Index Painting"
msgstr "Högupplöst indexmålning"

#: ../../reference_manual/filters/artistic.rst:16
msgid "Artistic"
msgstr "Konstnärlig"

#: ../../reference_manual/filters/artistic.rst:18
msgid ""
"The artistic filter are characterised by taking an input, and doing a "
"deformation on them."
msgstr ""
"De konstnärliga filtren kännetecknas av att de tar indata och utför en "
"deformering av det."

#: ../../reference_manual/filters/artistic.rst:24
msgid ".. image:: images/filters/Krita_halftone_filter.png"
msgstr ".. image:: images/filters/Krita_halftone_filter.png"

#: ../../reference_manual/filters/artistic.rst:25
msgid ""
"The halftone filter is a filter that converts the colors to a halftone dot "
"pattern."
msgstr ""
"Halvtonsfiltret är ett filter som konverterar färgerna till ett "
"halvtonspunktmönster."

#: ../../reference_manual/filters/artistic.rst:27
msgid "Colors"
msgstr "Färger"

#: ../../reference_manual/filters/artistic.rst:28
msgid ""
"The colors used to paint the pattern. The first is the color of the dots, "
"the second the color of the background."
msgstr ""
"Färgerna som används för att återge mönstret. Den första är punkternas färg, "
"den andra bakgrundens färg."

#: ../../reference_manual/filters/artistic.rst:29
msgid "Size"
msgstr "Storlek"

#: ../../reference_manual/filters/artistic.rst:30
msgid ""
"The size of the cell in pixels. The maximum dot size will be using the "
"diagonal as the cell size to make sure you can have pure black."
msgstr ""
"Cellstorleken i bildpunkter. Den maximala punktstorleken använder diagonalen "
"som punktstorlek för att säkerställa att man kan få rent svart."

#: ../../reference_manual/filters/artistic.rst:31
msgid "Angle"
msgstr "Vinkel"

#: ../../reference_manual/filters/artistic.rst:32
msgid "The angle of the dot pattern."
msgstr "Punktmönstrets vinkel."

#: ../../reference_manual/filters/artistic.rst:33
msgid "Invert"
msgstr "Invertera"

#: ../../reference_manual/filters/artistic.rst:34
msgid ""
"This inverts the intensity calculated per dot. Thus, dark colors will give "
"tiny dots, and light colors big dots. This is useful in combination with "
"inverting the colors, and give a better pattern on glowy-effects."
msgstr ""
"Inverterar intensiteten beräknad ber punkt. Sålunda ges mörka färger mycket "
"små punkter, och ljusa färger stora punkter. Det är användbart tillsammans "
"med att invertera färgerna, och ger ett bättre mönster för glödeffekter."

#: ../../reference_manual/filters/artistic.rst:36
msgid "Anti-aliasing"
msgstr "Kantutjämning"

#: ../../reference_manual/filters/artistic.rst:36
msgid ""
"This makes the dots smooth, which is good for webgraphics. Sometimes, for "
"print graphics, we want there to be no grays, so we turn off the anti-"
"aliasing."
msgstr ""
"Gör punkterna jämna, vilket är bra för webbgrafik. Ibland vill vi att det "
"inte ska finnas några gråa färger för utskriftsgrafik, så vi stänger av "
"kantutjämning."

#: ../../reference_manual/filters/artistic.rst:39
msgid "Index Color"
msgstr "Indexera färg"

#: ../../reference_manual/filters/artistic.rst:41
msgid ""
"The index color filter maps specific user selected colors to the grayscale "
"value of the artwork. You can see the example below, the strip below the "
"black and white gradient has index color applied to it so that the black and "
"white gradient gets the color selected to different values."
msgstr ""
"Filtret Indexera färg avbildar specifika färger utvalda av användaren till "
"grafikens gråskalevärde. Man kan se i exemplet nedan att remsan under den "
"svartvita toningen har indexerad färg, så att den svartvita toningen får "
"färger tilldelade till olika värden."

#: ../../reference_manual/filters/artistic.rst:44
msgid ".. image:: images/common-workflows/Gradient-pixelart.png"
msgstr ".. image:: images/common-workflows/Gradient-pixelart.png"

#: ../../reference_manual/filters/artistic.rst:45
msgid ""
"You can choose the required colors and ramps in the index color filter "
"dialog as shown below ."
msgstr ""
"Man kan välja nödvändiga färger och lutningarna i dialogrutan för Indexera "
"färg som visas nedan."

#: ../../reference_manual/filters/artistic.rst:48
msgid ".. image:: images/filters/Index-color-filter.png"
msgstr ".. image:: images/filters/Index-color-filter.png"

#: ../../reference_manual/filters/artistic.rst:49
msgid ""
"You can create index painting such as one shown below with the help of this "
"filter."
msgstr ""
"Man kan skapa en indexerad målning som den som visas nedan med hjälp av "
"filtret."

#: ../../reference_manual/filters/artistic.rst:52
msgid ".. image:: images/common-workflows/Kiki-pixel-art.png"
msgstr ".. image:: images/common-workflows/Kiki-pixel-art.png"

#: ../../reference_manual/filters/artistic.rst:54
msgid "Pixelize"
msgstr "Lägg till bildpunkter"

#: ../../reference_manual/filters/artistic.rst:56
msgid ""
"Makes the input-image pixely by creating small cells and inputting an "
"average color."
msgstr ""
"Gör indatabilden bildpunktsmässig genom att skapa små celler och ange ett "
"medelvärde av färgen."

#: ../../reference_manual/filters/artistic.rst:59
msgid ".. image:: images/filters/Pixelize-filter.png"
msgstr ".. image:: images/filters/Pixelize-filter.png"

#: ../../reference_manual/filters/artistic.rst:61
msgid "Raindrops"
msgstr "Regndroppar"

#: ../../reference_manual/filters/artistic.rst:63
msgid "Adds random raindrop-deformations to the input-image."
msgstr "Lägger till slumpmässiga regndroppsdeformeringar i indatabilden."

#: ../../reference_manual/filters/artistic.rst:66
msgid "Oilpaint"
msgstr "Oljemålning"

#: ../../reference_manual/filters/artistic.rst:68
msgid ""
"Does semi-posterisation to the input-image, with the 'brush-size' "
"determining the size of the fields."
msgstr ""
"Gör halvaffischering av indatabilden, där 'penselstorlek' bestämmer fältens "
"storlek."

#: ../../reference_manual/filters/artistic.rst:71
msgid ".. image:: images/filters/Oilpaint-filter.png"
msgstr ".. image:: images/filters/Oilpaint-filter.png"

#: ../../reference_manual/filters/artistic.rst:72
msgid "Brush-size"
msgstr "Penselstorlek"

#: ../../reference_manual/filters/artistic.rst:73
msgid ""
"Determines how large the individual patches are. The lower, the more "
"detailed."
msgstr ""
"Bestämmer hur stora de enskilda fläckarna är. Ju mindre, desto mer "
"detaljerade."

#: ../../reference_manual/filters/artistic.rst:75
msgid "Smoothness"
msgstr "Utjämning"

#: ../../reference_manual/filters/artistic.rst:75
msgid "Determines how much each patch's outline is smoothed out."
msgstr "Bestämmer hur mycket konturen av varje fläck jämnas ut."

#: ../../reference_manual/filters/artistic.rst:78
msgid "Posterize"
msgstr "Affischera"

#: ../../reference_manual/filters/artistic.rst:80
msgid ""
"This filter decreases the amount of colors in an image. It does this per "
"component (channel)."
msgstr ""
"Filtret minskar antal färger på en bild. Det gör det per komponent (kanal)."

#: ../../reference_manual/filters/artistic.rst:83
msgid ".. image:: images/filters/Posterize-filter.png"
msgstr ".. image:: images/filters/Posterize-filter.png"

#: ../../reference_manual/filters/artistic.rst:84
msgid ""
"The :guilabel:`Steps` parameter determines how many colors are allowed per "
"component."
msgstr ""
"Parametern :guilabel:`Steg` bestämmer hur många färger som tillåts per "
"komponent."
