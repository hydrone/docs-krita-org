msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-02 16:12-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../reference_manual/main_menu/tools_menu.rst:1
msgid "The tools menu in Krita."
msgstr ""

#: ../../reference_manual/main_menu/tools_menu.rst:11
msgid "Macro"
msgstr ""

#: ../../reference_manual/main_menu/tools_menu.rst:11
msgid "Scripts"
msgstr ""

#: ../../reference_manual/main_menu/tools_menu.rst:17
msgid "Tools Menu"
msgstr ""

#: ../../reference_manual/main_menu/tools_menu.rst:19
msgid "This contains three things."
msgstr ""

#: ../../reference_manual/main_menu/tools_menu.rst:22
msgid "Scripting"
msgstr ""

#: ../../reference_manual/main_menu/tools_menu.rst:24
msgid ""
"When you have python scripting enabled and have scripts toggled, this is "
"where most scripts are stored by default."
msgstr ""

#: ../../reference_manual/main_menu/tools_menu.rst:27
msgid "Recording"
msgstr ""

#: ../../reference_manual/main_menu/tools_menu.rst:31
msgid "The recording and macro features are unmaintained and buggy."
msgstr ""

#: ../../reference_manual/main_menu/tools_menu.rst:33
msgid ""
"Record a macro. You do this by pressing start, drawing something and then "
"pressing stop. This feature can only record brush strokes. The resulting "
"file is stored as a \\*.kritarec file."
msgstr ""

#: ../../reference_manual/main_menu/tools_menu.rst:36
msgid "Macros"
msgstr ""

#: ../../reference_manual/main_menu/tools_menu.rst:38
msgid ""
"Play back or edit a krita rec file. The edit can only change the brush "
"preset on strokes or add and remove filters."
msgstr ""
