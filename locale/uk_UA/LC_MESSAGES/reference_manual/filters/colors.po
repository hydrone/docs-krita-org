# Translation of docs_krita_org_reference_manual___filters___colors.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___filters___colors\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:35+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/filters/colors.rst:None
msgid ".. image:: images/filters/Krita-color-to-alpha.png"
msgstr ".. image:: images/filters/Krita-color-to-alpha.png"

#: ../../reference_manual/filters/colors.rst:1
msgid "Overview of the color filters."
msgstr "Огляд фільтрів кольорів."

#: ../../reference_manual/filters/colors.rst:11
msgid "Filters"
msgstr "Фільтри"

#: ../../reference_manual/filters/colors.rst:16
msgid "Color"
msgstr "Колір"

#: ../../reference_manual/filters/colors.rst:18
msgid ""
"Similar to the Adjust filters, the color filters are image wide color "
"operations."
msgstr ""
"Подібно до фільтрів коригування, фільтри кольорів є діями над кольорами "
"усього зображення."

#: ../../reference_manual/filters/colors.rst:20
#: ../../reference_manual/filters/colors.rst:24
msgid "Color to Alpha"
msgstr "Колір до альфи"

#: ../../reference_manual/filters/colors.rst:26
msgid ""
"This filter allows you to make one single color transparent (alpha). By "
"default when you run this filter white is selected, you can choose a color "
"that you want to make transparent from the color selector."
msgstr ""
"За допомогою цього фільтра можна замінити певний колір на прозорість (альфа-"
"канал). Типово, у цьому фільтрі вибрано білий колір. Ви можете вибрати "
"колір, який хочете зробити прозорим, за допомогою засобу вибору кольору."

#: ../../reference_manual/filters/colors.rst:29
msgid ".. image:: images/filters/Color-to-alpha.png"
msgstr ".. image:: images/filters/Color-to-alpha.png"

#: ../../reference_manual/filters/colors.rst:30
msgid ""
"The Threshold indicates how much other colors will be considered mixture of "
"the removed color and non-removed colors. For example, with threshold set to "
"255, and the removed color set to white, a 50% gray will be considered a "
"mixture of black+white, and thus transformed in a 50% transparent black."
msgstr ""
"За допомогою параметра :guilabel:`Поріг` можна визначити, наскільки інші "
"кольори вважатимуться сумішшю вилученого кольору і кольорів, які "
"лишатимуться на зображенні. Наприклад, якщо встановлено значення порогу 255, "
"а вилученим кольором є білий, 50% сірий вважатиметься сумішшю чорного і "
"білого кольорів, отже, його буде перетворено на прозорий на 50% чорний."

#: ../../reference_manual/filters/colors.rst:36
msgid ""
"This filter is really useful in separating line art from the white "
"background."
msgstr "Цей фільтр є корисним для відокремлення графіки від білого тла."

#: ../../reference_manual/filters/colors.rst:41
msgid "Color Transfer"
msgstr "Перенесення кольорів"

#: ../../reference_manual/filters/colors.rst:43
msgid ""
"This filter converts the colors of the image to colors from the reference "
"image. This is a quick way to change a color combination of an artwork to an "
"already saved image or a reference image."
msgstr ""
"Цей фільтр перетворює кольори зображення на кольори з еталонного зображення. "
"Це швидкий спосіб змінити комбінацію кольорів твору на комбінацію кольорів "
"вже збереженого зображення або комбінацію кольорів еталонного зображення."

#: ../../reference_manual/filters/colors.rst:47
msgid ".. image:: images/filters/Color-transfer.png"
msgstr ".. image:: images/filters/Color-transfer.png"

#: ../../reference_manual/filters/colors.rst:51
msgid "Maximize Channel"
msgstr "Максимізувати канал"

#: ../../reference_manual/filters/colors.rst:53
msgid ""
"This filter checks for all the channels of a each single color and set all "
"but the highest value to 0."
msgstr ""
"Цей фільтр обробляє усі канали усіх кольорів і усі значення, окрім "
"найбільшого, замінюються на 0."

#: ../../reference_manual/filters/colors.rst:58
msgid "Minimize Channel"
msgstr "Мінімізувати канал"

#: ../../reference_manual/filters/colors.rst:60
msgid ""
"This is reverse to Maximize channel, it checks all the channels of a each "
"single color and sets all but the lowest to 0."
msgstr ""
"Обернена дія щодо максимізації каналу: виконується обробка усіх каналів усіх "
"кольорів і усі значення, окрім найменшого, замінюються на 0."
